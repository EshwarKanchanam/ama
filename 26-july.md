## What is the difference between (==) and (===) operator ?

=== — strict equality (triple equals) - checks for equality without conversion of data type.

== — loose equality (double equals) - checks for equality do type conversion if necessary.

### === — strict equality (triple equals)

```
const num = 0;
const obj = new String("0");
const str = "0";

console.log(num === num); // true
console.log(obj === obj); // true
console.log(str === str); // true

console.log(num === obj); // false
console.log(num === str); // false
console.log(obj === str); // false
console.log(null === undefined); // false
console.log(obj === null); // false
console.log(obj === undefined); // false
```

### == — loose equality (double equals)

```
const num = 0;
const big = 0n;
const str = "0";
const obj = new String("0");

console.log(num == str); // true
console.log(big == num); // true
console.log(str == big); // true

console.log(num == obj); // true
console.log(big == obj); // true
console.log(str == obj); // true

```

## Console.log(typeof []) is equals to 'object' why?

An array can be considered to be an object with the following properties/keys:

1. Length - This can be 0 or above (non-negative).
2. The array indices. By this, I mean "0", "1", "2", etc are all properties of array object.

## Whats are the disadvantages of using closure

Closures capture variables and chains of scope/contexts. As long as closures are active, this memory cannot be garbage collected. Therefore, closures can lead to memory leaks if not used well.

## Why using global variables are bad in js ?

- **Naming collision**
- **Risk of Overwriting Variables**
  - Since global variables can be accessed and modified from anywhere, there's a high risk of accidentally overwriting these variables. This can lead to severe bugs that are hard to trace.
- **Memory Consumption**
  - Global variables remain in memory for as long as the webpage is loaded. This could lead to higher memory consumption, especially if you're dealing with large amounts of data.
- **Code Maintenance**
  - Global variables can be modified from anywhere in the code, making it difficult to track changes. This can lead to unexpected behavior in your code, making debugging a nightmare.

## How to extract certain properties from an array of objects to create a new array ?

Using map higher order function we can extract certain properties from an array of objects

```
let arrayOfObjects = [
  { name: "abhi", age: 2 },
  { name: "eshwar", age: 23 },
];

let ageArray = arrarrayOfObjects.map(person => person.age); //[24,23]

```

## Difference between charAt() and at() method in js ?

Both can be used to find character at a particular position

charAt() - will only take +values return string, returns empty string if index position is not present.

at() - will take +values and -values(to get char from the end of the string) return string, return undefined if index position is not present.

|           method           | nonExistedPosition |
| :------------------------: | :----------------: |
| charAt(nonExistedPosition) |         ''         |
|   at(nonExistedPosition)   |     undefined      |

```
let arr = 'Hi';

console.log(arr.charAt(5)); // ''
console.log(arr.at(5)); //undefined
```

## How to reset initial or 1st commit in git, if its possible ?

We can delete the HEAD and restore your repository to a new state, where we can create a new initial commit:

```
git update-ref -d HEAD
```

After we create a new commit, if you have already pushed to remote, we will need to force it to the remote in order to overwrite the previous initial commit:

```
git push --force origin
```

## console.log(typeof []) returns 'object', then why can not we use (.) operator in array to accesss its values just like objects,where we use (.) operator in objects for accessing key and values

The dot notation (.) in JavaScript is reserved for accessing properties of objects by their fixed, known names. Since arrays do not have named properties in the same way objects do, dot notation is not applicable for accessing array elements.

## Why 'forEach' loop skips 'not defined' whereas 'for' loop gives 'undefined' ?

forEach loop will skip not defined values because it uses hasOwnProperty() to check for defined value.
It return true if value is defined for an index otherwise false.

for loop gives undefined for not defined values because we are accessing the value which is not defined.
We can also skip empty or not defined places in sparse array by using hasOwnProperty() of Object.

## How can we mimic the action 'Array.splice' and 'Array.slice' on objects

While directly mimicking Array.splice and Array.slice on objects isn't possible due to their inherent structure, we can achieve similar functionality using alternative approaches:

1. **Object Destructuring for "Slice" behavior:**

Object destructuring allows you to extract properties into variables. You can use this to create a new object containing a subset of properties from the original object.

Here's an example:

```
const person = { name: "Alice", age: 30, city: "New York" };
const { name, age } = person; // Creates new object with only name and age

console.log(name, age); // Output: Alice 30
```

2. **Spread operator (...) for variations:**

The spread operator (...) can be used to create shallow copies of objects. You can combine it with destructuring to achieve different slicing effects:

- **Slice from start:**

```
const person = { name: "Alice", age: 30, city: "New York" };
const slicedPerson = { ...person }; // Shallow copy
delete slicedPerson.city; // Remove city property

console.log(slicedPerson); // { name: "Alice", age: 30 }
```

- **Slice from specific index (mimicking behavior, not literal index):**

This requires iterating through the object's properties based on a counter or condition.

```
const person = { name: "Alice", age: 30, city: "New York" };
let slicedPerson = {};
let counter = 0; // Mimicking index

for (const key in person) {
  if (counter >= 1) { // Start from index 1 (skipping name)
    slicedPerson[key] = person[key];
  }
  counter++;
}

console.log(slicedPerson); // { age: 30, city: "New York" }
```

3. **Object.assign for selective copying:**

This allows you to copy properties from one or more source objects to a target object.

```
const person = { name: "Alice", age: 30, city: "New York" };
const slicedPerson = {};
Object.assign(slicedPerson, person, { city: undefined }); // Copy all except city

console.log(slicedPerson); // { name: "Alice", age: 30 }
```

## What is "Callback Hell" problem and how to avoid it ?

Callback hell refers to the situation in asynchronous programming where code becomes difficult to read and maintain due to multiple nested callbacks. This typically happens when handling asynchronous operations such as making HTTP requests, reading files, or interacting with databases, where each operation requires a callback function to handle the result or error.

Example for callback hell:

```
asyncOperation1(function(result1, err1) {
    if (err1) {
        handleError(err1);
    } else {
        asyncOperation2(function(result2, err2) {
            if (err2) {
                handleError(err2);
            } else {
                asyncOperation3(function(result3, err3) {
                    if (err3) {
                        handleError(err3);
                    } else {
                        // and so on...
                    }
                });
            }
        });
    }
});

```

To avoid it we can use :

1. **Named Functions** :
   Instead of defining anonymous functions directly inside callbacks, define named functions for each operation. This helps in organizing the code and making it more readable.

```
asyncOperation1(function callback1(result1, err1) {
    if (err1) {
        handleError(err1);
    } else {
        asyncOperation2(callback2);
    }
});

function callback2(result2, err2) {
    if (err2) {
        handleError(err2);
    } else {
        asyncOperation3(callback3);
    }
}

function callback3(result3, err3) {
    if (err3) {
        handleError(err3);
    } else {
        // Continue with the next steps
    }
}

```

2. **Use Promises**:
   Promises provide a more structured way to handle asynchronous operations and avoid deeply nested callbacks. Most modern JavaScript libraries and frameworks support promises natively.

```
asyncOperation1()
    .then(result1 => asyncOperation2())
    .then(result2 => asyncOperation3())
    .then(result3 => {
        // Continue with the next steps
    })
    .catch(err => {
        handleError(err);
    });

```

3. **Use Async/Await**:
   Async functions and the await keyword provide an even cleaner and more synchronous-looking way to write asynchronous code, especially useful when dealing with multiple asynchronous operations.

```
async function performOperations() {
    try {
        const result1 = await asyncOperation1();
        const result2 = await asyncOperation2();
        const result3 = await asyncOperation3();
        // Continue with the next steps
    } catch (err) {
        handleError(err);
    }
}

performOperations();

```

**Benefits of Avoiding Callback Hell:**

- **Readability**: Code becomes more readable and easier to understand.
- **Maintainability**: Easier to maintain and debug.
- **Scalability**: Handles complex asynchronous operations more effectively.
- **Error Handling**: Centralized error handling becomes simpler and more effective.

## why "Hello" is also printing ? when we have exported array('ar') only!

```
//file1.js

let ar = [1,2,3];
console.log("Hello");
module.exports.ar = ar;

//file2.js
const {ar} = require('file1.js');
let print = ar;
console.log(print);

Output: "Hello"
        [1,2,3]
```

The "Hello" is logged because console.log("Hello"); is executed at the time file1.js is loaded, regardless of whether ar is accessed or not in file2.js. This behavior is normal for how Node.js modules work: code in the module file runs as soon as it is loaded or required by another module.
